package com.apparnest.adapter;

import java.util.List;

import com.apparnest.binchecker.R;
import com.apparnest.model.Feature;

import android.R.bool;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawerAdapter extends ArrayAdapter<Feature> {

	public Context context;

	public NavDrawerAdapter(Context context, int textViewResourceId,
			List<Feature> items) {
		super(context, textViewResourceId, items);
		Log.e("msg", "" + items.size());
		this.context = context;

	}

	private class ViewHolder {

		TextView name;
		ImageView icons;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
			int type = getItemViewType(position);
			if (type == 0) {

				convertView = mInflater.inflate(R.layout.nav_drawer_row, null);
				holder = new ViewHolder();
				holder.name = (TextView) convertView
						.findViewById(R.id.textViewop);
				holder.icons = (ImageView) convertView
						.findViewById(R.id.imageView1);
				convertView.setTag(holder);

			}

		} else
			holder = (ViewHolder) convertView.getTag();

		Feature f=getItem(position);
		
		holder.name.setText(f.getName());
		holder.icons.setImageResource(f.getId());
		return convertView;

	}

}
