package com.apparnest.adapter;

import java.util.List;

import com.apparnest.binchecker.AboutActivity;
import com.apparnest.binchecker.EmailSenderActivity;
import com.apparnest.binchecker.MainActivity;
import com.apparnest.binchecker.R;
import com.apparnest.model.EmailRow;
import com.apparnest.model.Feature;

import android.R.bool;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EmailAdapter extends ArrayAdapter<EmailRow> {

	public Context context;

	public EmailAdapter(Context context, int textViewResourceId,
			List<EmailRow> items) {
		super(context, textViewResourceId, items);
		Log.e("msg", "" + items.size());
		this.context = context;

	}

	private class ViewHolder {

		TextView name;
		TextView name_mail;
		TextView number_start;
		ImageView iv;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
			int type = getItemViewType(position);
			if (type == 0) {

				convertView = mInflater.inflate(R.layout.row_email, null);
				holder = new ViewHolder();
				holder.name = (TextView) convertView
						.findViewById(R.id.tv_region);
				holder.name_mail = (TextView) convertView
						.findViewById(R.id.tv_email);
				holder.number_start = (TextView) convertView
						.findViewById(R.id.tv_fisrt_two);
				holder.iv = (ImageView) convertView
						.findViewById(R.id.iv_email);
				
				convertView.setTag(holder);

			}

		} else
			holder = (ViewHolder) convertView.getTag();

		final EmailRow er=getItem(position);
		
		holder.name.setText(er.getName());
		holder.name_mail.setText(er.getEmail());
		holder.number_start.setText(er.getNumber());
		
		
		return convertView;

	}

}
