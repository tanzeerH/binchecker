package com.apparnest.binchecker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.crypto.spec.PSource;

import com.apparnest.adapter.SimpleListAdapter;
import com.apparnest.model.UserPicture;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.splunk.mint.Mint;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.opengl.ETC1;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class EmailSenderActivity extends Activity {
	private static final int SELECT_PICTURE = 1;
	private static final int CAMERA_REQUEST = 2;
	private String selectedImagePath;
	private Uri outputFileUri;
	private UserPicture userPic;
	private Bitmap picture;
	private ArrayList<Bitmap> imageList = new ArrayList<Bitmap>();
	private ArrayList<ImageView> ivList = new ArrayList<ImageView>();
	private ImageView iv1, iv2, iv3, iv4, iv5;
	private int MAX_IMAGE = 5;
	private Button addPhoto, btnSendEmail;
	private TextView tvLocation;
	private EditText etName, etAddress, etDescription;

	private double txt_size = 0.8;
	Typeface font;
	String location = "";
	String email_address = "";
	String file="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Mint.initAndStartSession(EmailSenderActivity.this, "285fb1e0");
		setContentView(R.layout.activity_email_send);
		super.onCreate(savedInstanceState);
		

		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle("Complain");

		tvLocation = (TextView) findViewById(R.id.tv_location);
		etName = (EditText) findViewById(R.id.et_name);
		etAddress = (EditText) findViewById(R.id.et_address);
		etDescription = (EditText) findViewById(R.id.et_descrition);

		iv1 = (ImageView) findViewById(R.id.iv1);
		iv2 = (ImageView) findViewById(R.id.iv2);
		iv3 = (ImageView) findViewById(R.id.iv3);
		iv4 = (ImageView) findViewById(R.id.iv4);
		iv5 = (ImageView) findViewById(R.id.iv5);

		addPhoto = (Button) findViewById(R.id.btn_photos);

		addPhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (imageList.size() >= MAX_IMAGE)
					Utility.alert(EmailSenderActivity.this,
							"Maximum image limit is " + MAX_IMAGE);
				else
					showOptionsDialog();

			}
		});
		btnSendEmail = (Button) findViewById(R.id.btn_send_mail);
		btnSendEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String name = etName.getText().toString();
				String address = etAddress.getText().toString();
				String des = etDescription.getText().toString();
				if (name.length() == 0) {
					SpannableString string = AndroidCustomFontSupport
							.getCorrectedBengaliFormat(
									"প্রতিষ্ঠানের নাম উল্লেখ করা হয়নি।", font,
									(float) .8);
					Utility.alert2(EmailSenderActivity.this, string);
				} else

				if (address.length() == 0) {
					SpannableString string = AndroidCustomFontSupport
							.getCorrectedBengaliFormat(
									"প্রতিষ্ঠানের ঠিকানা উল্লেখ করা হয়নি।",
									font, (float) .8);
					Utility.alert2(EmailSenderActivity.this, string);
				} else if (des.length() == 0) {
					SpannableString string = AndroidCustomFontSupport
							.getCorrectedBengaliFormat(
									"অভিযোগের বিবরন  উল্লেখ করা হয়নি।", font,
									(float) .8);
					Utility.alert2(EmailSenderActivity.this, string);
				} else
					convertoImageFiles(name, address, des);

			}
		});
		font = Typeface.createFromAsset(getAssets(),
				"fonts/solaimanlipinormal.ttf");

		SpannableString string = AndroidCustomFontSupport
				.getCorrectedBengaliFormat("প্রতিষ্ঠানের নাম", font, (float) .8);
		etName.setHint(string);
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				"প্রতিষ্ঠানের ঠিকানা", font, (float) .8);
		etAddress.setHint(string);
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				"অভিযোগের বিবরন ", font, (float) .8);
		etDescription.setHint(string);
		
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				"ছবি সংযুক্ত করুন ", font, (float) .8);
		
		addPhoto.setText(string);
		
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				"ইমেইল করুন ", font, (float) .8);
		
		btnSendEmail.setText(string);
		
		
		getIntentData();
		populateViewList();
	}

	private void getIntentData() {
		Intent i = getIntent();
		if (i != null) {
			int pos = i.getIntExtra("position", -1);
			if (pos != -1) {
				location = getResources().getStringArray(R.array.list_regions)[pos];
				SpannableString string = AndroidCustomFontSupport
						.getCorrectedBengaliFormat("প্রতিষ্ঠানের অবস্থান  :"
								+ location, font, (float) .8);
				email_address = getResources().getStringArray(
						R.array.list_mails)[pos];
				tvLocation.setText(string);
			}
		}
	}

	private void populateViewList() {
		ivList.add(iv1);
		ivList.add(iv2);
		ivList.add(iv3);
		ivList.add(iv4);
		ivList.add(iv5);
		setCallBacks();
	}

	private void setCallBacks() {
		for (int i = 0; i < MAX_IMAGE; i++) {
			ivList.get(i).setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					int pos = getViewPosition(v);
					if (pos != -1 && pos < imageList.size()) {
						alertType4(EmailSenderActivity.this, pos);

					}
					return false;
				}
			});
			ivList.get(i).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int pos = getViewPosition(v);
					if (pos != -1 && pos < imageList.size()) {
						Utility.bitmap = imageList.get(pos);

						Intent i = new Intent(EmailSenderActivity.this,
								SampleActivity.class);
						i.putExtra("position", 0);
						startActivity(i);
					}
				}
			});
		}
	}

	private int getViewPosition(View v) {
		for (int i = 0; i < MAX_IMAGE; i++) {
			if (v == ivList.get(i))
				return i;
		}
		return -1;
	}

	private void deleteImages(int pos) {
		imageList.remove(pos);
		addImagesToViews();
	}

	void prepareCamera() {
		final String dir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
				+ "/VAT_Registration_Checker/";
		File newdir = new File(dir);
		newdir.mkdirs();
		file = dir + System.currentTimeMillis() + ".jpg";
		File newfile = new File(file);
		try {
			newfile.createNewFile();
		} catch (IOException e) {
		}

		outputFileUri = Uri.fromFile(newfile);

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

		startActivityForResult(cameraIntent, CAMERA_REQUEST);
	}

	private void alertType4(final Context context, final int pos) {

		AlertDialog.Builder bld = new AlertDialog.Builder(context,
				AlertDialog.THEME_HOLO_LIGHT);
		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle("Delete");
		bld.setMessage("Do you want to delete this image?");
		bld.setCancelable(false);
		bld.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.dismiss();
				deleteImages(pos);

			}
		});
		bld.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.dismiss();
				

			}
		});

		bld.create().show();
	}

	private void showOptionsDialog() {
		final Dialog dialog = new Dialog(EmailSenderActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_attatchment);
		ArrayList<String> list = new ArrayList<String>();
		list.add("Take Photo");
		list.add("Choose from existing photos.");
		list.add("Cancel");

		ListView lv = (ListView) dialog.findViewById(R.id.lv_attatch);
		SimpleListAdapter adapter = new SimpleListAdapter(
				EmailSenderActivity.this, R.layout.row_simple_list, list);

		lv.setAdapter(adapter);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();

		// wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);
		dialog.show();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				if (position == 0) {
					prepareCamera();
				} else if (position == 1) {
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(
							Intent.createChooser(intent, "Select Picture"),
							SELECT_PICTURE);
				}
				dialog.dismiss();
			}
		});

	}

	private void convertoImageFiles(String name, String address, String des) {

		ArrayList<String> pathList = new ArrayList<String>();
		final String dir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
				+ "/VAT_Registration_Checker/";
		File newdir = new File(dir);
		newdir.mkdirs();

		for (int i = 0; i < imageList.size(); i++) {
			try {
				// Write file
				Bitmap bmp = imageList.get(i);
				String filename = dir + System.currentTimeMillis() + "_" + i
						+ ".jpg";
				pathList.add(filename);
				File file = new File(filename);
				file.createNewFile();

				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				bmp.compress(CompressFormat.JPEG, 100, bos);
				byte[] bitmapdata = bos.toByteArray();

				// write the bytes in file
				FileOutputStream fos = new FileOutputStream(file);
				fos.write(bitmapdata);
				fos.flush();
				fos.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
		email.setType("text/plain");
		ArrayList<Uri> uris = new ArrayList<Uri>();
		for (int i = 0; i < pathList.size(); i++) {
			File fileIn = new File(pathList.get(i));
			Uri u = Uri.fromFile(fileIn);
			uris.add(u);
		}
		String subject = "মূসক সংক্রান্ত অভিযোগ।";
		String message = "প্রতিষ্ঠানের নাম :" + name + "\n"
				+ "প্রতিষ্ঠানের ঠিকানা :" + address + "\n" + "অভিযোগের বিবরন :"
				+ des;
		email.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
		email.putExtra(Intent.EXTRA_EMAIL, new String[] { email_address });
		email.putExtra(Intent.EXTRA_TEXT, message);
		email.putExtra(Intent.EXTRA_SUBJECT, subject);
		startActivity(email);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				Uri selectedImageUri = data.getData();
				userPic = new UserPicture(selectedImageUri,
						getContentResolver());
				try {
					picture = userPic.getBitmap();
					if (picture != null)
						selectedImagePath = userPic.getPath();
				} catch (IOException e) {

					e.printStackTrace();
				}

				if (picture != null) {

					imageList.add(picture);
					addRecentImages();

				}
				System.out.println(selectedImagePath);
				// profilePhotoUrl = selectedImagePath;

			}
		}
		if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
			Log.e("msg", "picture saved");

			//userPic = new UserPicture(outputFileUri, getContentResolver());
			
				BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		        bmpFactoryOptions.inJustDecodeBounds = false; 
		      picture=  BitmapFactory.decodeFile(file, bmpFactoryOptions);
				//if (picture != null)
					//selectedImagePath = userPic.getPath();
			
			if (picture != null) {

				imageList.add(picture);
				addRecentImages();
			} else {
				Log.e("ms", "null");
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void addImagesToViews() {

		for (int i = 0; i < imageList.size(); i++)
			ivList.get(i).setImageBitmap(imageList.get(i));
		for (int i = imageList.size(); i < MAX_IMAGE; i++) {
			ivList.get(i).setImageResource(R.drawable.placeholder);
		}

	}

	private void addRecentImages() {
		int size = imageList.size();

		if (size > 0) {
			ivList.get(size - 1).setImageBitmap(imageList.get(size - 1));
			;
		}
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.prev_slide_in, R.anim.prev_slide_out);
		// super.onBackPressed();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {

			finish();
			overridePendingTransition(R.anim.prev_slide_in,
					R.anim.prev_slide_out);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
