package com.apparnest.binchecker;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;



import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class AboutActivity extends Activity {
	
	TextView tv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle("About");
		tv=(TextView)findViewById(R.id.tv_about);
		
		Typeface font=Typeface.createFromAsset(getAssets(), "fonts/solaimanlipinormal.ttf");
		
		SpannableString string=AndroidCustomFontSupport.getCorrectedBengaliFormat(getResources().getString(R.string.about_info),font,(float) 1.2);
		tv.setText(string);
	}
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.prev_slide_in, R.anim.prev_slide_out);
		//super.onBackPressed();
	}
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			
			finish();
			overridePendingTransition(R.anim.prev_slide_in, R.anim.prev_slide_out);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
