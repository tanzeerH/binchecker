package com.apparnest.binchecker;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class SampleActivity extends Activity{
	
	private ImageView iv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_sample);
		super.onCreate(savedInstanceState);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		iv=(ImageView)findViewById(R.id.iv_sample);
		getIntenData();
	}
	private void getIntenData()
	{
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fonts/solaimanlipinormal.ttf");
		
		Intent i=getIntent();
		if(i!=null)
		{
			int x=i.getIntExtra("position",-1);
			if(x!=-1)
			{
				iv.setImageBitmap(Utility.bitmap);
				SpannableString string = AndroidCustomFontSupport
						.getCorrectedBengaliFormat("ছবি  ", font,
								(float) 1.2);
				getActionBar().setTitle(string);
			}
			
		}
		else
		{
			iv.setImageResource(R.drawable.sample);
			SpannableString string = AndroidCustomFontSupport
					.getCorrectedBengaliFormat("নমুনা নিবন্ধন সনদ ", font,
							(float) 1.2);
			getActionBar().setTitle(string);
		}
	}
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.prev_slide_in, R.anim.prev_slide_out);
		//super.onBackPressed();
	}
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			
			finish();
			overridePendingTransition(R.anim.prev_slide_in, R.anim.prev_slide_out);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
