package com.apparnest.binchecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.apparnest.adapter.NavDrawerAdapter;
import com.apparnest.fragment.BINCheckerFragment;
import com.apparnest.model.Feature;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.splunk.mint.Mint;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends FragmentActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;

	private ActionBarDrawerToggle mDrawerToggle;
	private NavDrawerAdapter adapter;
	private ArrayList<Feature> fList = new ArrayList<Feature>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Mint.initAndStartSession(MainActivity.this, "285fb1e0");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		populateListView();
		adapter = new NavDrawerAdapter(MainActivity.this,
				R.layout.nav_drawer_row, fList);
		mDrawerList.setAdapter(adapter);
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				mDrawerLayout.closeDrawers();
				if (position == 0)
					mDrawerLayout.closeDrawers();
				else if (position == 1) {
					Intent i = new Intent(MainActivity.this, FAQActivity.class);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				}else if (position == 2) {
					Intent i = new Intent(MainActivity.this,
							ComplianActivity.class);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				} 
				else if (position == 3) {
					Intent i = new Intent(MainActivity.this,
							SampleActivity.class);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				} else if (position == 4) {
					Intent i = new Intent(MainActivity.this,
							AboutActivity.class);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				}

			}
		});
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.nav_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(
				// getResources().getString(R.string.app_name));

				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				// getActionBar().setTitle("");
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		
		BINCheckerFragment binCheckerFragment=new BINCheckerFragment();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.frame_container, binCheckerFragment).commit();


	}
	@Override
	protected void onResume() {
		
		super.onResume();
	}

	private void populateListView() {
		fList.add(new Feature("Home", R.drawable.home));
		fList.add(new Feature("FAQ", R.drawable.faq));
		fList.add(new Feature("Complain", R.drawable.complain));
		fList.add(new Feature("Sample", R.drawable.example));
		fList.add(new Feature("About", R.drawable.about));

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return true;

	}

}
