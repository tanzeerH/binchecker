package com.apparnest.binchecker;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.SpannableString;
import android.view.WindowManager.BadTokenException;

public class Utility {
	
	public static Bitmap bitmap=null;
	public static boolean hasInternet(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
	public static ProgressDialog createProgressDialog(Context mContext) {
		ProgressDialog dialog = new ProgressDialog(mContext);
		try {
			dialog.show();
		} catch (BadTokenException e) {

		}
		dialog.setCancelable(false);
		dialog.setContentView(R.layout.progress_dialog);
		// dialog.setMessage(Message);
		return dialog;
	}
	public static void alert(Activity context, String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(context);
		bld.setTitle("BIN Checker");
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setNeutralButton("Ok", null);
		bld.create().show();
	}
	public static void alert2(Activity context, SpannableString message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(context);
		bld.setTitle("BIN Checker");
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setNeutralButton("Ok", null);
		bld.create().show();
	}
}
