package com.apparnest.binchecker;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class FAQActivity extends Activity {

	private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10;
	private Button btnSample, btnComplain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faq);

		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle("Frequently Asked Questions");

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fonts/solaimanlipinormal.ttf");

		SpannableString string = AndroidCustomFontSupport
				.getCorrectedBengaliFormat(
						getResources().getString(R.string.qa1), font,
						(float) 1.2);
		tv1 = (TextView) findViewById(R.id.tv1);
		tv2 = (TextView) findViewById(R.id.tv2);
		tv3 = (TextView) findViewById(R.id.tv3);
		tv4 = (TextView) findViewById(R.id.tv4);
		tv5 = (TextView) findViewById(R.id.tv5);
		tv6 = (TextView) findViewById(R.id.tv6);
		tv7 = (TextView) findViewById(R.id.tv7);
		tv8 = (TextView) findViewById(R.id.tv8);
		tv9 = (TextView) findViewById(R.id.tv9);
		tv10 = (TextView) findViewById(R.id.tv10);
		btnComplain = (Button) findViewById(R.id.btn_complain);
		btnSample = (Button) findViewById(R.id.btn_sample);

		tv1.setText(string);

		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa2), font, (float) 1.2);

		tv2.setText(string);

		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa3), font, (float) 1.2);

		tv3.setText(string);

		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa4), font, (float) 1.2);

		tv4.setText(string);

		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa5), font, (float) 1.2);

		tv5.setText(string);
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa6), font, (float) 1.2);

		tv6.setText(string);
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa7), font, (float) 1.2);

		tv7.setText(string);
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa8), font, (float) 1.2);

		tv8.setText(string);
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa9), font, (float) 1.2);

		tv9.setText(string);
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				getResources().getString(R.string.qa10), font, (float) 1.2);

		tv10.setText(string);

		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				"নমুনা সনদ দেখতে ক্লিক করুন।", font, (float) 1);
		btnSample.setText(string);
		string = AndroidCustomFontSupport.getCorrectedBengaliFormat(
				"অভিযোগ করতে ক্লিক করুন।", font, (float) 1);
		btnComplain.setText(string);

		btnSample.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(FAQActivity.this, SampleActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

			}
		});
		btnComplain.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(FAQActivity.this, ComplianActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

			}
		});

	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.prev_slide_in, R.anim.prev_slide_out);
		// super.onBackPressed();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {

			finish();
			overridePendingTransition(R.anim.prev_slide_in,
					R.anim.prev_slide_out);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
