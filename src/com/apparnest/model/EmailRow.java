package com.apparnest.model;

import android.text.SpannableString;

public class EmailRow {
   private SpannableString name;
   private String email;
   private SpannableString number;
public SpannableString getName() {
	return name;
}
public void setName(SpannableString name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public SpannableString getNumber() {
	return number;
}
public void setNumber(SpannableString number) {
	this.number = number;
}
public EmailRow(SpannableString name, String email, SpannableString number) {
	super();
	this.name = name;
	this.email = email;
	this.number = number;
}
   
}
