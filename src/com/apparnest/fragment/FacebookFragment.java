package com.apparnest.fragment;

import com.apparnest.binchecker.R;
import com.apparnest.binchecker.Utility;
import com.apparnest.binchecker.WebActivity;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;



public class FacebookFragment extends Fragment {

	private TextView tv;
	private Typeface font;
	private Button btn;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_facebook, null, false);
		tv=(TextView)v.findViewById(R.id.tv);
		btn=(Button)v.findViewById(R.id.btn_fb);
		font = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/solaimanlipinormal.ttf");
		
		SpannableString string=AndroidCustomFontSupport.getCorrectedBengaliFormat(getResources().getString(R.string.complain_to_text),font,(float) .8);
		tv.setText(string);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Utility.hasInternet(getActivity()))
				{
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/dhakanorthvat?fref=ts&ref=br_tf"));
				startActivity(browserIntent);
				}
				else
					Utility.alert(getActivity(),"Please check your internet connection.");
				
			}
		});
		return v;

	}
	
}
