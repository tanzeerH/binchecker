package com.apparnest.fragment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.apparnest.binchecker.ComplianActivity;
import com.apparnest.binchecker.R;
import com.apparnest.binchecker.R.menu;
import com.apparnest.binchecker.Utility;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.SyncStateContract.Constants;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BINCheckerFragment extends Fragment {

	private EditText et;
	private Button btnSearch;
	private TextView tv, tv_results;
	private ProgressDialog progressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_bin_checker, null, false);

		et = (EditText) v.findViewById(R.id.et);
		btnSearch = (Button) v.findViewById(R.id.btn_search);
		tv = (TextView) v.findViewById(R.id.tv_top);
		tv_results = (TextView) v.findViewById(R.id.tv_result);
		makeInvisible();
		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (et.getText().toString().equals("")) {
					Utility.alert(getActivity(), "Please insert a BIN number.");
				} else if (et.getText().toString().length() != 11)
					alertType4(getActivity(),
							"Please insert a valid BIN number.Valid BIN number contains 11 digits"
							+ ".But this BIN number contains "+et.getText().toString().length()+" digits.Please CHECK BIN number again.If it really contains "
									+  et.getText().toString().length()+" digits then it is not a valid BIN number and you can complain using this app. ",et.getText().toString());

				else if (Utility.hasInternet(getActivity())) {
					progressDialog = Utility
							.createProgressDialog(getActivity());

					new GetBINStatus(et.getText().toString()).execute();
				} else
					Utility.alert(getActivity(),
							"Please check your  internet connection.");
			}

		});
		return v;
	}

	private void setTextAndVisisble(String message) {
		tv_results.setVisibility(View.VISIBLE);
		tv_results.setText("Result: " + message);
	}

	private void makeInvisible() {
		tv_results.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// setting fonts
		Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/solaimanlipinormal.ttf");

		SpannableString string = AndroidCustomFontSupport
				.getCorrectedBengaliFormat(
						getResources().getString(R.string.header), font,
						(float) 1);
		tv.setText(string);
		super.onActivityCreated(savedInstanceState);
	}

	private class GetBINStatus extends AsyncTask<Void, Void, Void> {

		InputStream IS;
		private String bin;
		private String response = "";

		public GetBINStatus(String b) {
			this.bin = b;
		}

		@Override
		protected Void doInBackground(Void... params) {
			Log.e("here", "here we");
			String url = "http://www.nbr.gov.bd/getbinfield.php";
			HttpClient clinet = new DefaultHttpClient();

			HttpPost httppost = new HttpPost(url);
			ArrayList<NameValuePair> key = new ArrayList<NameValuePair>();
			key.add(new BasicNameValuePair("txtSearch", bin));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(key));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				HttpResponse response = clinet.execute(httppost);
				HttpEntity entity = response.getEntity();
				IS = entity.getContent();

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			BufferedReader bf = new BufferedReader(new InputStreamReader(IS));
			String line = "";

			try {
				while ((line = bf.readLine()) != null) {
					response += line;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.e("res", "abc" + response);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (progressDialog.isShowing())
				progressDialog.dismiss();
			if (response.length() != 0) {

				response = Html.fromHtml(response).toString();
				String res = response.replaceAll("\\s+", " ");
				Log.e("msg", res);
				String[] arr = res.split("Enter BIN");
				if (arr.length == 0)
					Utility.alert(getActivity(), "An error occured.");
				else if (arr[arr.length - 1].contains("No Result Found"))
					alertType2(getActivity(), arr[arr.length - 1], bin);
				else
					alertType3(getActivity(), arr[arr.length - 1], bin);
			}
			super.onPostExecute(result);
		}
	}

	private void alertType2(final Context context, final String message,
			final String bin) {
		setTextAndVisisble(message);
		AlertDialog.Builder bld = new AlertDialog.Builder(context,
				AlertDialog.THEME_HOLO_LIGHT);

		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle("BIN (" + bin + ")");

		Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/solaimanlipinormal.ttf");

		SpannableString string = AndroidCustomFontSupport
				.getCorrectedBengaliFormat(
						message
								+ "\n"
								+ "যেহেতু প্রদত্ত BIN নাম্বার এর জন্য কোন তথ্য পাওয়া যায়নি তাই আপনি প্রতিষ্ঠানের নিবন্ধন সনদ দেখতে চান।মূল্য সংযোজন কর আইন অনুযায়ী নিবন্ধিত প্রতিটি প্রতিষ্ঠানে নিবন্ধন সনদ প্রকাশ্য স্থানে ঝুলিয়ে রাখার বাধ্যবাধকতা রয়েছে। যদি কর্তৃপক্ষ নিবন্ধন সনদ না দেখায় বা নিবন্ধন সনদ সংক্রান্ত কোনও অভিযোগ থাকে তাহলে তা সংশ্লিষ্ট মূসক কমিশনারেটে অভিযোগ করুন। ",
						font, (float) .8);

		bld.setMessage(string);
		bld.setCancelable(false);
		bld.setPositiveButton("Complain",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						makeInvisible();
						dialog.dismiss();
						Intent i = new Intent(getActivity(),
								ComplianActivity.class);
						startActivity(i);
						getActivity().overridePendingTransition(
								R.anim.slide_in, R.anim.slide_out);

					}
				});
		bld.setNegativeButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				makeInvisible();
				dialog.dismiss();

			}
		});
		bld.setNeutralButton("Screenshot",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						takeScreenshot();

					}
				});

		bld.create().show();
	}

	private void alertType3(final Context context, final String message,
			final String bin) {
		setTextAndVisisble(message);
		AlertDialog.Builder bld = new AlertDialog.Builder(context,
				AlertDialog.THEME_HOLO_LIGHT);
		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle("BIN (" + bin + ")");
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setPositiveButton("Screenshot",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						takeScreenshot();
						
					}
				});
		bld.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				makeInvisible();
				dialog.dismiss();

			}
		});
		bld.create().show();

	}

	private void shareImage(String path) {

		Uri uri = Uri.parse("file:///" + path);
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		// shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
		// "Test Mail");
		shareIntent
				.putExtra(
						android.content.Intent.EXTRA_TEXT,
						"App link: "
								+ "https://play.google.com/store/apps/details?id=com.apparnest.binchecker");
		shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
		startActivity(Intent.createChooser(shareIntent, "Share Via"));
	}

	private void alertType4(final Context context, final String message,
			final String bin) {
		setTextAndVisisble("Invalid BIN number.Valid BIN number contains 11 digits.");
		AlertDialog.Builder bld = new AlertDialog.Builder(context,
				AlertDialog.THEME_HOLO_LIGHT);

		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle("BIN (" + bin + ")");

		Typeface font = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/solaimanlipinormal.ttf");

		

		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setPositiveButton("Complain",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						makeInvisible();
						dialog.dismiss();
						Intent i = new Intent(getActivity(),
								ComplianActivity.class);
						startActivity(i);
						getActivity().overridePendingTransition(
								R.anim.slide_in, R.anim.slide_out);

					}
				});
		bld.setNegativeButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				makeInvisible();
				dialog.dismiss();

			}
		});
		bld.setNeutralButton("Screenshot",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						takeScreenshot();

					}
				});

		bld.create().show();
	}

	private void takeScreenshot() {
		String time = "" + System.currentTimeMillis();

		try {
			// image naming and path to include sd card appending name you
			// choose for file
			final String dir = Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
					+ "/VAT_Registration_Checker/";
			File newdir = new File(dir);
			newdir.mkdirs();
			String filePath = dir + "VAT_REG_CHECKER_"+System.currentTimeMillis() + ".jpg";

			// create bitmap screen capture
			View v1 = getActivity().getWindow().getDecorView().getRootView();
			v1.setDrawingCacheEnabled(true);
			Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
			v1.setDrawingCacheEnabled(false);

			File imageFile = new File(filePath);

			FileOutputStream outputStream = new FileOutputStream(imageFile);
			int quality = 100;
			bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
			outputStream.flush();
			outputStream.close();

			makeInvisible();
			MediaScannerConnection.scanFile(getActivity(),
					new String[] { imageFile.getPath() },
					new String[] { "image/jpeg" }, null);
			Toast.makeText(getActivity(), "Screenshot taken successfully.",
					Toast.LENGTH_SHORT).show();
			// alertType4(getActivity(),mPath);
			// openScreenshot(imageFile);
		} catch (Throwable e) {
			makeInvisible();
			Utility.alert(getActivity(), "An error occured.");
			e.printStackTrace();
		}
	}

	private void openScreenshot(File imageFile) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		Uri uri = Uri.fromFile(imageFile);
		intent.setDataAndType(uri, "image/*");
		startActivity(intent);
	}
}
