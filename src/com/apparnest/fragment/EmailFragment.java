package com.apparnest.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.apparnest.adapter.EmailAdapter;
import com.apparnest.binchecker.EmailSenderActivity;
import com.apparnest.binchecker.R;
import com.apparnest.model.EmailRow;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;



public class EmailFragment extends Fragment{
	
	private ListView lv;
	private TextView tv;
	private EmailAdapter adapter;
	private ArrayList<EmailRow> list=new ArrayList<EmailRow>();
	private List<String> regionList=new ArrayList<String>();
	private List<String> mailList=new ArrayList<String>();
	private List<String> codeList=new ArrayList<String>();
	private Typeface font;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_email, null, false);
		tv=(TextView)v.findViewById(R.id.tv);
		lv=(ListView)v.findViewById(R.id.lv_mails);
		font = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/solaimanlipinormal.ttf");
		
		SpannableString string=AndroidCustomFontSupport.getCorrectedBengaliFormat(getResources().getString(R.string.complain_to_mail),font,(float) .8);
		tv.setText(string);
		loadData();
		adapter=new EmailAdapter(getActivity(),R.layout.row_email,list);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent email = new Intent(Intent.ACTION_SEND);
				
				
				Intent i = new Intent(getActivity(),
						EmailSenderActivity.class);
				i.putExtra("position", position);
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			}
		});
		
		return v;
		}
	
	private void loadData()
	{
		
		regionList=Arrays.asList(getActivity().getResources().getStringArray(R.array.list_regions));
		mailList=Arrays.asList(getActivity().getResources().getStringArray(R.array.list_mails));
		codeList=Arrays.asList(getActivity().getResources().getStringArray(R.array.list_code));
		String commonStr=getResources().getString(R.string.bangla_number);
		
		for(int i=0;i<regionList.size();i++)
		{
			SpannableString region=AndroidCustomFontSupport.getCorrectedBengaliFormat(regionList.get(i),font,(float) 1);
			SpannableString code;
			if(i<11)
				code=AndroidCustomFontSupport.getCorrectedBengaliFormat(commonStr+ codeList.get(i),font,(float) 1);
			else
				code=AndroidCustomFontSupport.getCorrectedBengaliFormat(codeList.get(i),font,(float) 1);
				
			list.add(new EmailRow(region,mailList.get(i),code));
		}
		
	}

}
